// Imports -----------------------------
import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import ReactDOM from "react-dom";
import { HashRouter, Route } from "react-router-dom";
import "shards-ui/dist/css/shards.min.css";
import "./index.css";
import NavExample from "./Navbar";
import NewReview from "./NewReview";
import NewToken from "./NewToken";
import Profile from "./Profile";
import { fetchFeed, fetchProfile } from "./utils/service";
import Feed from "./Feed";
import { web3DataFetch } from "./utils/utils";

class Main extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      posts: [],
      contract: null,
      accounts: null,
      web3: null,
      open: false,
    };

    this.fetchData = this.fetchData.bind(this);
  }

  fetchData = async () => {
    if (!this.state.contract || !this.state.accounts) {
      return;
    }

    const responses = await fetchFeed(this.state.contract, this.state.accounts);

    this.setState({
      posts: responses,
    });
  };

  componentDidMount = async () => {
    const obj = await web3DataFetch();

    let provider = obj.web3.currentProvider;

    this.setState({
      web3: obj.web3,
      accounts: obj.accounts,
      contract: obj.contract,
    });

    await this.fetchData();
  };

  render() {
    return (
      <div>
        <NavExample accounts={this.state.accounts} />
        <HashRouter>
          <Route
            exact
            path="/"
            component={(props) => (
              <Feed
                {...props}
                posts={this.state.posts}
                accounts={this.state.accounts}
                contract={this.state.contract}
                web3={this.state.web3}
                fetchData={this.fetchData}
              />
            )}
          />
          <Route
            path="/new-review"
            component={(props) => (
              <NewReview
                {...props}
                accounts={this.state.accounts}
                contract={this.state.contract}
                fetchData={this.fetchData}
              />
            )}
          />
          <Route path="/new-token" component={NewToken} />
          <Route
            path="/profile"
            component={(props) => (
              <Profile
                {...props}
                posts={this.state.posts}
                accounts={this.state.accounts}
                contract={this.state.contract}
                profile={this.state.profile}
                fetchData={this.fetchData}
              />
            )}
          />
          <Route
            path="/profile/:address"
            component={(props) => (
              <Profile
                {...props}
                posts={this.state.posts}
                accounts={this.state.accounts}
                contract={this.state.contract}
                profile={this.state.profile}
                fetchData={this.fetchData}
                web3={this.state.web3}
              />
            )}
          />
        </HashRouter>
      </div>
    );
  }
}

ReactDOM.render(<Main />, document.getElementById("root"));
