import { gql, GraphQLClient } from "graphql-request";

export const GET_CONNECTIONS = gql`
  query ($address: String!, $first: Int) {
    identity(address: $address) {
      followings(first: $first) {
        list {
          address
          domain
        }
      }
      followers(first: $first) {
        list {
          address
          domain
        }
      }
    }
  }
`;

export const GET_IDENTITY = gql`
  query FullIdentityQuery($address: String!) {
    identity(address: $address, network: ETH) {
      address
      domain
      avatar
      joinTime
      twitter {
        handle
        avatar
        verified
        tweetId
        source
        followerCount
      }
      github {
        username
        gistId
        userId
      }
      followerCount(namespace: "CyberConnect")
      followingCount(namespace: "CyberConnect")
      followings(namespace: "CyberConnect", first: 2, after: "-1") {
        pageInfo {
          startCursor
          endCursor
          hasNextPage
          hasPreviousPage
        }
        list {
          address
          domain
          avatar
          alias
          namespace
          lastModifiedTime
          verifiable
        }
      }
      followers(namespace: "CyberConnect", first: 2, after: "-1") {
        pageInfo {
          startCursor
          endCursor
          hasNextPage
          hasPreviousPage
        }
        list {
          address
          domain
          avatar
          alias
          namespace
          lastModifiedTime
          verifiable
        }
      }
      friends(namespace: "CyberConnect", first: 2, after: "-1") {
        pageInfo {
          startCursor
          endCursor
          hasNextPage
          hasPreviousPage
        }
        list {
          address
          domain
          avatar
          alias
          namespace
          lastModifiedTime
          verifiable
        }
      }
    }
  }
`;

export const CONNECTION_STATUS = gql`
  query ProofQuery($address: String!, $postAddress: String!) {
    connections(fromAddr: $address, toAddrList: [$postAddress], network: ETH) {
      fromAddr
      toAddr
      followStatus {
        isFollowed
        isFollowing
      }
      namespace
      alias
      network
      createdAt
      updatedAt
      proof
    }
  }
`;
