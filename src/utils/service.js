import { GraphQLClient } from "graphql-request";
import { CONNECTION_STATUS, GET_IDENTITY } from "./subgraph";
import { ConnectionType } from "@cyberlab/cyberconnect";

const client = new GraphQLClient("https://api.cybertino.io/connect/");

export const fetchFeed = async (contract, accounts) => {
  let count = await contract.methods.getPostCount().call();
  let responses = [];
  for (let i = 0; i < count; i++) {
    let response = await contract.methods.getPosts(i).call();
    let isPostLiked = await isLiked(accounts[0], response.nftAddress);
    let isPostReported = await isReported(accounts[0], response.nftAddress);

    const profile = await fetchProfile(response.addr, response.addr);

    response.isLiked = isPostLiked;
    response.isReported = isPostReported;
    response.profile = profile;

    responses.push(response);
  }

  console.log("responses: ", responses);
  return responses;
};

export const fetchProfile = async (address, profileAddress) => {
  if (!profileAddress || !address) {
    return;
  }
  const res = await client.request(GET_IDENTITY, {
    address: profileAddress,
  });
  if (profileAddress === address) {
    res.isFollowing = false;
  } else {
    const isFollowing = await isFollowed(address, profileAddress);
    res.isFollowing = isFollowing;
  }

  return res;
};

export const isLiked = async (address, postAddress) => {
  const res = await client.request(CONNECTION_STATUS, {
    address,
    postAddress,
  });

  if (
    res.connections[0].alias === ConnectionType.LIKE &&
    res.connections[0].followStatus.isFollowing === true
  ) {
    return true;
  } else {
    return false;
  }
};

export const isReported = async (address, postAddress) => {
  const res = await client.request(CONNECTION_STATUS, {
    address,
    postAddress,
  });

  if (
    res.connections[0].alias === ConnectionType.REPORT &&
    res.connections[0].followStatus.isFollowing === true
  ) {
    return true;
  } else {
    return false;
  }
};

export const isFollowed = async (address, followAddress) => {
  const res = await client.request(CONNECTION_STATUS, {
    address,
    postAddress: followAddress,
  });

  if (
    res.connections[0].alias === ConnectionType.FOLLOW &&
    res.connections[0].followStatus.isFollowing === true
  ) {
    return true;
  } else {
    return false;
  }
};
