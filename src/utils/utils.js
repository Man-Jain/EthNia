import getWeb3 from "./getWeb3.js";
import Travel from "../contracts/Travel.json";

let web3 = null;
let accounts = null;
let contract = null;

export const web3Fetch = () => web3;
export const accountsFetch = () => accounts;
export const contractFetch = async () => {
  if (!contract) {
    await getContractInstance();
  }

  return contract;
};

export const web3DataFetch = async () => {
  if (!contract) {
    await getContractInstance();
  }

  return {
    web3: web3,
    accounts: accounts,
    contract: contract,
  };
};

const getContractInstance = async () => {
  try {
    // Get network provider and web3 instance.
    web3 = await getWeb3();

    // Use web3 to get the user's accounts.
    accounts = await web3.eth.getAccounts();

    // Get the contract instance.
    const networkId = await web3.eth.net.getId();
    console.log("networkId: ", networkId);
    const deployedNetwork = Travel.networks[networkId];

    let contractAddress;

    if (networkId === 80001) {
      contractAddress = "0x609D0a1b36b7B2dfF130aa452A6Ee91d461E1266";
    } else if (networkId === 1313161554) {
      contractAddress = "0x06c69fa0e25dede0d25865fa795448fa4851dfb9";
    } else if (networkId === 1287) {
      contractAddress = "0xde7f37bbe563fb19de1e11e2449fccaa88cd1ba9";
    } else {
      contractAddress = "0x609D0a1b36b7B2dfF130aa452A6Ee91d461E1266";
    }

    contract = new web3.eth.Contract(Travel.abi, contractAddress);

    const obj = {
      web3: web3,
      accounts: accounts,
      contract: contract,
    };

    return obj;
  } catch (error) {
    alert(
      `Failed to load web3, accounts, or contract. Check console for details.`
    );
    console.error(error);
    return error;
  }
};

export const shortenAddress = (address) => {
  return (
    address.substring(0, 5) + "..." + address.substring(address.length - 5)
  );
};

export default getContractInstance;
