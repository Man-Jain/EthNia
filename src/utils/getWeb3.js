import Web3 from "web3";
import Web3Modal from "web3modal";
import WalletConnectProvider from "@walletconnect/web3-provider";

let provider;

const providerOptions = {
  /* See Provider Options Section */
  walletconnect: {
    package: WalletConnectProvider, // required
    options: {
      infuraId: "67531e96ca3842cdabf3147f5d2a3742", // required
    },
  },
};

const web3Modal = new Web3Modal({
  network: "mainnet", // optional
  cacheProvider: true, // optional
  providerOptions, // required
});

const getWeb3 = () =>
  new Promise(async (resolve, reject) => {
    provider = await web3Modal.connect();
    console.log("provider: ", provider);
    const web3 = new Web3(provider);

    // Subscribe to provider connection

    if (
      parseInt(provider.chainId) !== 80001 &&
      parseInt(provider.chainId) !== 1313161554 &&
      parseInt(provider.chainId) !== 1287
    ) {
      console.log("Here");
      alert(
        "Please make sure you are connected to one of the supported networks on Metamask - NEAR Mainnet, Moonbase Alpha or Polygon Mumbai"
      );
      console.log("Here2");
      window.location.reload();
    }

    resolve(web3);

    provider.on("connect", (info) => {
      console.log("info: ", info);
      if (
        parseInt(info.chainId) !== 80001 &&
        parseInt(info.chainId) !== 1313161554 &&
        parseInt(info.chainId) !== 1287
      ) {
        console.log("Here");
        alert(
          "Please make sure you are connected to one of the supported networks on Metamask - NEAR Mainnet, Moonbase Alpha or Polygon Mumbai"
        );
        console.log("Here2");
        window.location.reload();
      }
    });

    provider.on("chainChanged", (chainId) => {
      window.location.reload();
    });

    // Wait for loading completion to avoid race conditions with web3 injection timing.
    // window.addEventListener("load", async () => {
    //   // Modern dapp browsers...
    //   if (window.ethereum) {
    //     const web3 = new Web3(window.ethereum);
    //     try {
    //       // Request account access if needed
    //       await window.ethereum.enable();
    //       // Acccounts now exposed
    //       resolve(web3);
    //     } catch (error) {
    //       reject(error);
    //     }
    //   }
    //   // Legacy dapp browsers...
    //   else if (window.web3) {
    //     // Use Mist/MetaMask's provider.
    //     const web3 = window.web3;
    //     console.log("Injected web3 detected.");
    //     resolve(web3);
    //   }
    //   // Fallback to localhost; use dev console port by default...
    //   else {
    //     const provider = new Web3.providers.HttpProvider(
    //       "https://moonbeam-alpha.api.onfinality.io/public"
    //     );
    //     const web3 = new Web3(provider);
    //     console.log("No web3 instance injected, using Local web3.");
    //     resolve(web3);
    //   }
    // });
  });

export default getWeb3;
