import React from "react";
import getContractInstance, { web3DataFetch } from "./utils/utils.js";
import Travel from "./contracts/Travel.json";
import HomeImage from "./assets/HomeImage.png";

import {
  Row,
  Col,
  Container,
  Button,
  FormInput,
  Collapse,
  Card,
  CardFooter,
  CardBody,
  CardTitle,
  CardHeader,
  CardImg,
  Badge,
  Modal,
  ModalBody,
  ModalHeader,
} from "shards-react";
import { fetchFeed } from "./utils/service.js";

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      posts: props.posts,
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({ open: !this.state.open });
  }

  handleVote = async (event) => {
    const action = event.target.value;
    const { accounts, contract } = this.props.match.params;
    if (action === "yes") {
      await contract.methods
        .upvote(event.target.id)
        .send({ from: accounts[0] });

      alert("Your Vote Done");
      window.location.reload();
    } else {
      await contract.methods
        .downvote(event.target.id)
        .send({ from: accounts[0] });

      alert("Your Vote Done");
      window.location.reload();
    }
  };

  componentDidMount = () => {
    console.log("this.props.posts: ", this.props.posts);
  };

  render() {
    return (
      <div>
        <Container className="main-container">
          <Row style={{ display: "flex", alignItems: "center" }}>
            <Col>
              <img
                style={{ width: "45rem", height: "30rem" }}
                src={HomeImage}
              />
            </Col>
            <Col>
              <h2 className="home-title"> Welcome To Ethnicity!</h2>
              <p className="home-subtitle">
                A community of travellers, proof of presence on an awesome
                place, incentivised support from local people and a review
                system based on reputation, all these combined would give us an
                opportunity to get all the nooks and corners of the city
                explored by tourists.
              </p>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
