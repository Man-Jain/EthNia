import React from "react";
import getContractInstance, { shortenAddress } from "./utils/utils.js";
import Travel from "./contracts/Travel.json";

import {
  Row,
  Col,
  Container,
  Button,
  FormInput,
  Collapse,
  Card,
  CardFooter,
  CardBody,
  CardTitle,
  CardHeader,
  CardImg,
  ListGroup,
  ListGroupItem,
} from "shards-react";
import { GraphQLClient } from "graphql-request";
import { GET_IDENTITY } from "./utils/subgraph.js";
import Ens from "./assets/ens.png";
import Twitter from "./assets/twitter.png";
import { UserMinus, UserPlus } from "react-feather";
import { ConnectionType } from "@cyberlab/cyberconnect";
import CyberConnect, { Env, Blockchain } from "@cyberlab/cyberconnect";
import { fetchProfile } from "./utils/service.js";

export default class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: {},
      kudos: [],
      posts: props.posts,
      profile2: null,
      accounts: props.accounts,
      web3: props.web3,
    };

    this.toggle = this.toggle.bind(this);
    this.runExample = this.runExample.bind(this);
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  runExample = async () => {
    const { accounts, contract } = this.props;
    if (contract) {
      let profile = await contract.methods
        .myProfile()
        .call({ from: this.props.match.params.address });
      let kudos = [];
      let kcount = await contract.methods
        .tokenid()
        .call({ from: this.props.match.params.address });
      for (let k = 0; k < kcount; k++) {
        let owner = await contract.methods
          .ownerOf(k)
          .call({ from: this.props.match.params.address });
        if (owner == this.props.match.params.address) {
          let tokenurli = await contract.methods
            .tokenURI(k)
            .call({ from: this.props.match.params.address });
          kudos.push(tokenurli);
        }
      }
      this.setState({ profile: profile, kudos: kudos });
    }
  };

  handleFollow = async () => {
    const account = this.props.match.params.address;
    const cyberConnect = new CyberConnect({
      namespace: "EthNia",
      env: Env.PRODUCTION,
      chain: Blockchain.ETH,
      provider: this.state.web3.currentProvider,
      signingMessageEntity: "ETHNIA",
    });

    try {
      const resp = await cyberConnect.connect(account, ConnectionType.FOLLOW);
    } catch (error) {
      console.log("error: ", error);
      await cyberConnect.disconnect(account);
    }
    await this.getProfileData();
  };

  getProfileData = async () => {
    await this.runExample();
    const profile2 = await fetchProfile(
      this.state.accounts ? this.state.accounts[0] : null,
      this.props.match.params.address
    );
    this.setState({ profile2 });
  };

  connectTwitter = async () => {
    const account = this.props.match.params.address;
    const cyberConnect = new CyberConnect({
      namespace: "EthNia",
      env: Env.PRODUCTION,
      chain: Blockchain.ETH,
      provider: this.state.web3.currentProvider,
      signingMessageEntity: "ETHNIA",
    });

    try {
      const resp = await cyberConnect.connect(account, ConnectionType.TWITTER);
    } catch (error) {
      console.log("error: ", error);
      await cyberConnect.disconnect(account);
    }
    await this.getProfileData();
  };

  componentDidMount = async () => {
    await this.getProfileData();
  };

  render() {
    const kudosList = this.state.kudos.map((item, index) => (
      <Col sm="12" md="3">
        <Card>
          <CardHeader>{JSON.parse(item).name}</CardHeader>
          <CardImg width="285" src={JSON.parse(item).image} />
          <CardBody>
            <p>{JSON.parse(item).description}</p>
          </CardBody>
        </Card>
      </Col>
    ));
    const listItems = this.state.posts.map((item) => (
      <Col>
        {this.state.profile2 ? (
          <ListGroupItem style={{ marginTop: "2rem" }}>
            <div style={{ display: "flex" }}>
              <CardImg
                width="50"
                height="50"
                src={
                  this.state.profile2.identity.avatar === ""
                    ? "https://c.gitcoin.co/avatars/0357f94b529985a8a898ab338add0edf/djrosenbaum.png"
                    : this.state.profile2.identity.avatar
                }
              />
              {/* <CardImg
            width="50"
            src={"https://ipfs.io/ipfs/" + JSON.parse(item.content).ipfsId}
          /> */}
              <div style={{ marginLeft: "1rem" }}>
                {this.state.profile2 ? (
                  <h3
                    style={{ textAlign: "left", marginBottom: "0rem" }}
                    className="title"
                  >
                    <b>
                      {this.state.profile2.identity.twitter.handle === ""
                        ? this.state.profile2.identity.domain === ""
                          ? shortenAddress(this.state.profile2.identity.address)
                          : this.state.profile2.identity.domain
                        : this.state.profile2.identity.twitter.handle}
                    </b>
                  </h3>
                ) : null}
                <CardTitle
                  style={{ textAlign: "left" }}
                  className="handle-title"
                >
                  <a
                    href={
                      this.state.profile2.identity.twitter.handle === ""
                        ? null
                        : `https://twitter.com/${this.state.profile2.identity.twitter.handle}`
                    }
                    target="_blank"
                  >{`@${
                    this.state.profile2.identity.twitter.handle === ""
                      ? this.state.profile2.identity.domain === ""
                        ? shortenAddress(this.state.profile2.identity.address)
                        : this.state.profile2.identity.domain
                      : this.state.profile2.identity.twitter.handle
                  }`}</a>
                </CardTitle>
              </div>
            </div>
            <p style={{ marginBottom: "0.75rem" }}>
              <span className="title-span">Reviewer:</span>{" "}
              {this.state.profile2.identity.domain === ""
                ? shortenAddress(this.state.profile2.identity.address)
                : this.state.profile2.identity.domain}
            </p>

            <p style={{ marginBottom: "0.75rem" }}>
              <span className="title-span">Place Location:</span>
              <a
                href={`https://www.google.com/maps/search/?api=1&query=${
                  JSON.parse(item.location).lat
                },${JSON.parse(item.location).lng}`}
              >
                {" "}
                Check on Maps
              </a>
            </p>
            <p style={{ marginBottom: "0.75rem" }}>
              <span className="title-span"> Subject:</span>{" "}
              {JSON.parse(item.content).subject}
            </p>
            <Button
              style={{ float: "right", marginTop: "-4rem" }}
              className="up-btn"
            >
              See More
            </Button>
          </ListGroupItem>
        ) : null}
      </Col>
    ));
    return (
      <div>
        {this.state.profile2 ? (
          <Container className="main-container">
            <div>
              <Row>
                <Col style={{ marginTop: "5rem" }} sm="15" md="4" lg="4">
                  {this.state.profile2 ? (
                    <>
                      <CardImg
                        style={{
                          width: "40%",
                          backgroundColor: "white",
                          border: "1px solid #e9e9e9",
                        }}
                        className="place-card"
                        src={
                          this.state.profile2.identity.avatar === ""
                            ? "https://c.gitcoin.co/avatars/0357f94b529985a8a898ab338add0edf/djrosenbaum.png"
                            : this.state.profile2.identity.avatar
                        }
                      />{" "}
                      <h3
                        style={{
                          textAlign: "left",
                          marginTop: "2rem",
                          marginBottom: "0rem",
                        }}
                        className="title"
                      >
                        <b>
                          {this.state.profile2.identity.twitter.handle === ""
                            ? this.state.profile2.identity.domain === ""
                              ? shortenAddress(
                                  this.state.profile2.identity.address
                                )
                              : this.state.profile2.identity.domain
                            : this.state.profile2.identity.twitter.handle}
                        </b>
                      </h3>
                      <CardTitle
                        style={{ textAlign: "left" }}
                        className="handle-title"
                      >
                        <a
                          href={
                            this.state.profile2.identity.twitter.handle === ""
                              ? null
                              : `https://twitter.com/${this.state.profile2.identity.twitter.handle}`
                          }
                          target="_blank"
                        >{`@${
                          this.state.profile2.identity.twitter.handle === ""
                            ? this.state.profile2.identity.domain === ""
                              ? shortenAddress(
                                  this.state.profile2.identity.address
                                )
                              : this.state.profile2.identity.domain
                            : this.state.profile2.identity.twitter.handle
                        }`}</a>
                      </CardTitle>
                      <Row style={{ width: "15rem" }}>
                        <Col>
                          <span className="title-span">
                            {this.state.profile2.identity.followingCount}
                          </span>
                          <h4 style={{ fontSize: "14px" }}>Following</h4>
                        </Col>
                        <Col>
                          <span className="title-span">
                            {this.state.profile2.identity.followerCount}
                          </span>
                          <h4 style={{ fontSize: "14px" }}>Followers</h4>
                        </Col>
                      </Row>
                      {this.state.accounts &&
                      this.state.accounts[0] !==
                        this.props.match.params.address ? (
                        <button
                          onClick={this.handleFollow}
                          className={
                            this.state.profile2.isFollowing
                              ? "down-btn-active"
                              : "down-btn"
                          }
                        >
                          {this.state.profile2.isFollowing ? (
                            <UserMinus
                              style={{
                                marginRight: "0.3rem",
                                marginTop: "-0.2rem",
                              }}
                              size={"16"}
                            />
                          ) : (
                            <UserPlus
                              style={{
                                marginRight: "0.3rem",
                                marginTop: "-0.2rem",
                              }}
                              size={"16"}
                            />
                          )}
                          {this.state.profile2.isFollowing
                            ? "Unfollow"
                            : "Follow"}
                        </button>
                      ) : null}
                      <p
                        className="title-span"
                        style={{
                          width: "20rem",
                          fontWeight: "400",
                          textAlign: "justify",
                          margin: "1rem 0rem",
                        }}
                      >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua.
                      </p>
                      <p style={{ marginBottom: "0.25rem" }}>
                        <span className="title-span"> Total Posts:</span>{" "}
                        {this.state.profile.posts}
                      </p>
                      <p style={{ marginBottom: "0.25rem" }}>
                        <span className="title-span">Total Kudos: </span>
                        {this.state.profile.tokens}
                      </p>
                      <p style={{ marginBottom: "0.25rem" }}>
                        <span className="title-span"> Total Upvotes:</span>{" "}
                        {this.state.profile.upvotes}
                      </p>
                      <p style={{ marginBottom: "0.25rem" }}>
                        <span className="title-span"> Total Downvotes:</span>{" "}
                        {this.state.profile.downvotes}
                      </p>
                      <hr />
                      <h3
                        style={{ marginBottom: "0rem", fontWeight: "500" }}
                        className="title-span"
                      >
                        #{" "}
                        {this.state.profile2.identity.address.substring(0, 5) +
                          "..." +
                          this.state.profile2.identity.address.substring(
                            this.state.profile2.identity.address.length - 5
                          )}
                      </h3>
                      <h3
                        style={{ marginBottom: "0rem", fontWeight: "500" }}
                        className="title-span"
                      >
                        <img
                          style={{ width: "12px", marginRight: "0.3rem" }}
                          src={Ens}
                        />
                        <span>
                          {this.state.profile2.identity.domain === ""
                            ? "Not Connected"
                            : this.state.profile2.identity.domain}
                        </span>
                      </h3>
                      <h3
                        style={{ marginBottom: "0rem", fontWeight: "500" }}
                        className="title-span"
                      >
                        <img
                          style={{ width: "14px", marginRight: "0.3rem" }}
                          src={Twitter}
                        />
                        {this.state.profile2.identity.twitter.handle === "" ? (
                          <span
                            onClick={this.connectTwitter}
                            style={{ cursor: "pointer" }}
                          >
                            Not Connected (Connect Now)
                          </span>
                        ) : (
                          <span style={{ cursor: "pointer" }}>
                            {this.state.profile2.identity.twitter.handle}
                          </span>
                        )}
                      </h3>
                    </>
                  ) : null}
                </Col>
                <Col sm="15" md="4" lg="8">
                  <h5 className="title">Places You have Added</h5>
                  <Row>
                    <ListGroup>{listItems}</ListGroup>
                  </Row>

                  <br />
                  <hr />
                  <h5 className="title">Your Kudos</h5>
                  <Row>{kudosList}</Row>
                  <Row></Row>
                </Col>
              </Row>
            </div>
          </Container>
        ) : null}
      </div>
    );
  }
}
