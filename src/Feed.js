import React from "react";
import getContractInstance, {
  shortenAddress,
  web3DataFetch,
} from "./utils/utils.js";
import Travel from "./contracts/Travel.json";
import Home from "./Home.js";
import {
  Row,
  Col,
  Container,
  Button,
  FormInput,
  Collapse,
  Card,
  CardFooter,
  CardBody,
  CardTitle,
  CardHeader,
  CardImg,
  Badge,
  Modal,
  ModalBody,
  ModalHeader,
} from "shards-react";
import { fetchFeed, isLiked } from "./utils/service.js";
import {
  MessageSquare,
  ThumbsUp,
  ThumbsDown,
  Flag,
  Eye,
  ArrowUp,
  Save,
  Bookmark,
  ArrowDown,
} from "react-feather";
import { ConnectionType } from "@cyberlab/cyberconnect";
import CyberConnect, { Env, Blockchain } from "@cyberlab/cyberconnect";

export default class Feed extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      posts: props.posts,
      web3: props.web3,
      contract: props.contract,
      accounts: props.accounts,
      selectedPost: null,
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({ open: !this.state.open });
  }

  handleVote = async (action, id) => {
    try {
      const { accounts, contract } = this.state;
      if (action === "yes") {
        await contract.methods.upvote(id).send({ from: accounts[0] });

        alert("Your Vote Done");
      } else {
        await contract.methods.downvote(id).send({ from: accounts[0] });

        alert("Your Vote Done");
      }
      this.props.fetchData();
    } catch (error) {}
  };

  handleLike = async (item) => {
    const nftAddress = item.nftAddress;
    console.log("nftAddress: ", nftAddress);

    if (item.isReported) {
      return;
    }

    const cyberConnect = new CyberConnect({
      namespace: "EthNia",
      env: Env.PRODUCTION,
      chain: Blockchain.ETH,
      provider: this.state.web3.currentProvider,
      signingMessageEntity: "ETHNIA",
    });

    try {
      const resp = await cyberConnect.connect(nftAddress, ConnectionType.LIKE);
      console.log("resp: ", resp);
    } catch (error) {
      console.log("error: ", error);
      await cyberConnect.disconnect(nftAddress);
    }
    this.props.fetchData();
  };

  handleReport = async (item) => {
    const nftAddress = item.nftAddress;
    console.log("nftAddress: ", nftAddress);
    const cyberConnect = new CyberConnect({
      namespace: "EthNia",
      env: Env.PRODUCTION,
      chain: Blockchain.ETH,
      provider: this.state.web3.currentProvider,
      signingMessageEntity: "ETHNIA",
    });

    try {
      const resp = await cyberConnect.connect(
        nftAddress,
        ConnectionType.REPORT
      );
      console.log("resp: ", resp);
    } catch (error) {
      console.log("error: ", error);
      await cyberConnect.disconnect(nftAddress);
      await cyberConnect.connect(nftAddress, ConnectionType.REPORT);
      console.log("Done Report");
    }
    this.props.fetchData();
  };

  componentDidMount = () => {};

  render() {
    return (
      <div>
        <Home />
        <Container className="main-container">
          <h3 className="title">Awesome Places Near You !!!</h3> <br />
          <Row>
            <div style={{ marginLeft: "2.3rem" }}>
              {" "}
              {this.state.posts ? (
                <Row>
                  {this.state.posts.map((item, index) => (
                    <Col sm="15" md="4" lg="4" style={{ marginTop: "10px" }}>
                      <Card className="place-card">
                        <CardImg
                          className="card-img"
                          src={
                            "https://arweave.net/" +
                            JSON.parse(item.content).arweaveHash
                          }
                        />
                        <CardBody className="card-body">
                          <div style={{ display: "flex" }}>
                            <img
                              width="50"
                              height="50"
                              src={
                                item.profile.identity.avatar === ""
                                  ? "https://c.gitcoin.co/avatars/0357f94b529985a8a898ab338add0edf/djrosenbaum.png"
                                  : item.profile.identity.avatar
                              }
                            />

                            <div style={{ marginLeft: "1rem" }}>
                              <h3
                                style={{
                                  textAlign: "left",
                                  marginBottom: "0rem",
                                }}
                                className="title"
                              >
                                <b>
                                  {item.profile.identity.twitter.handle === ""
                                    ? item.profile.identity.domain === ""
                                      ? shortenAddress(
                                          item.profile.identity.address
                                        )
                                      : item.profile.identity.domain
                                    : item.profile.identity.twitter.handle}
                                </b>
                              </h3>

                              <CardTitle
                                style={{ textAlign: "left" }}
                                className="handle-title"
                              >
                                {" "}
                                {item.profile.identity.twitter.handle === ""
                                  ? item.profile.identity.domain === ""
                                    ? shortenAddress(
                                        item.profile.identity.address
                                      )
                                    : item.profile.identity.domain
                                  : item.profile.identity.twitter.handle}
                              </CardTitle>
                              <div className="btn-div">
                                <span
                                  // onClick={() => {
                                  //   this.handleLike(item.nftAddress);
                                  // }}
                                  onClick={() => this.handleVote("yes", index)}
                                  className="icon-span"
                                >
                                  <ArrowUp
                                    style={{
                                      marginRight: "0.3rem",
                                      marginLeft: "0rem",
                                    }}
                                    className="icon"
                                    size={"16"}
                                  />
                                  {JSON.parse(item.upvotes)}
                                </span>
                                <span className="icon-span">
                                  <ArrowDown
                                    style={{ marginRight: "0.3rem" }}
                                    className="icon"
                                    size={"16"}
                                    onClick={() => this.handleVote("no", index)}
                                  />
                                  {JSON.parse(item.downvotes)}
                                </span>
                                <span
                                  onClick={() => {
                                    this.handleLike(item);
                                  }}
                                  className="icon-span"
                                >
                                  <ThumbsUp
                                    fill={item.isLiked ? "#8247E5" : "white"}
                                    color={item.isLiked ? "#8247E5" : "#343a40"}
                                    className="icon"
                                    size={"16"}
                                  />{" "}
                                </span>

                                <span className="icon-span">
                                  <MessageSquare className="icon" size={"16"} />{" "}
                                </span>

                                <span
                                  onClick={() => {
                                    this.handleReport(item);
                                  }}
                                  className="icon-span"
                                >
                                  <Flag
                                    fill={item.isReported ? "#8247E5" : "white"}
                                    color={
                                      item.isReported ? "#8247E5" : "#343a40"
                                    }
                                    className="icon"
                                    size={"16"}
                                  />
                                </span>
                              </div>
                            </div>
                          </div>

                          <hr />
                          <h3 className="title">
                            {" "}
                            {JSON.parse(item.content).subject}
                          </h3>
                          <p className="home-subtitle">
                            Added By:{" "}
                            {item.profile.identity.twitter.handle === ""
                              ? item.profile.identity.domain === ""
                                ? shortenAddress(item.profile.identity.address)
                                : item.profile.identity.domain
                              : item.profile.identity.twitter.handle}
                          </p>

                          <p className="home-subtitle">
                            {JSON.parse(item.content)
                              .details.substr(0, 38)
                              .concat("...")}
                            <span
                              className="link-title"
                              onClick={() => {
                                this.toggle();
                                this.setState({ selectedPost: item });
                              }}
                            >
                              Read More
                            </span>
                          </p>
                          {/* <Badge theme="success">
                              Upvotes: {JSON.parse(item.upvotes)}
                            </Badge>
                            <Badge theme="danger">
                              Downvotes: {JSON.parse(item.downvotes)}
                            </Badge>
                            <br />
                            <br />
                            <div className="btn-div">
                              <Button
                                id={index}
                                className="up-btn"
                                value="yes"
                                onClick={this.handleVote}
                              >
                                It was Awesome
                              </Button>
                              <Button
                                id={index}
                                className="down-btn"
                                value="no"
                                onClick={this.handleVote}
                              >
                                Not that Good
                              </Button>
                            </div> */}
                        </CardBody>
                      </Card>
                    </Col>
                  ))}
                  {this.state.selectedPost ? (
                    <Modal
                      className="modal"
                      open={this.state.open}
                      toggle={this.toggle}
                    >
                      <ModalHeader>
                        {JSON.parse(this.state.selectedPost.content).subject}
                      </ModalHeader>
                      <ModalBody>
                        {JSON.parse(this.state.selectedPost.content).details}
                      </ModalBody>
                    </Modal>
                  ) : null}
                </Row>
              ) : null}
            </div>
          </Row>
        </Container>
      </div>
    );
  }
}
