import React from "react";
import {
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  FormInput,
  Collapse,
} from "shards-react";
import { HashRouter, NavLink } from "react-router-dom";
import logo from "./assets/logo.png";
import "./index.css";
import { Home, Plus, Zap, User, CreditCard, LogIn } from "react-feather";
export default class NavExample extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      collapseOpen: false,
      value: 0,
    };

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.navclick = this.navclick.bind(this);

    console.log("props: ", props);
    console.log(this.state);
  }

  toggleNavbar() {
    this.setState({
      ...this.state,
      ...{
        collapseOpen: !this.state.collapseOpen,
      },
    });
  }

  navclick() {
    this.setState({ value: Math.random() });
  }

  // componentDidMount = async () => {
  //   this.setState({
  //     accounts: this.props.accounts,
  //   });
  // };

  render() {
    return (
      <div>
        <HashRouter>
          <Navbar
            type="dark"
            theme="primary"
            expand="md"
            className="navbar-class"
          >
            <NavbarToggler onClick={this.toggleNavbar} />
            <img style={{ height: "4rem" }} src={logo} />

            <Collapse open={this.state.collapseOpen} navbar>
              <Nav navbar className="nav ml-auto">
                <NavItem className="nav-item">
                  <Home size={"16"} />
                  <NavLink
                    onClick={this.navlink}
                    active
                    to="/"
                    className="nav-link"
                  >
                    Home
                  </NavLink>
                </NavItem>
                <NavItem className="nav-item">
                  <Plus size={"16"} />
                  <NavLink
                    onClick={this.navlink}
                    to="/new-review"
                    className="nav-link"
                  >
                    Add Place
                  </NavLink>
                </NavItem>
                {/* <NavItem>
                  <NavLink onClick={this.navlink} to="/" className="nav-link">
                    Places Near Me
                  </NavLink>
                </NavItem> */}
                <NavItem className="nav-item">
                  <Zap size={"16"} />
                  <NavLink
                    onClick={this.navlink}
                    to="/new-token"
                    className="nav-link"
                  >
                    Get New Token
                  </NavLink>
                </NavItem>

                <NavItem>
                  {this.props.accounts ? (
                    <NavLink
                      to={`/profile/${this.props.accounts[0]}`}
                      className="nav-link"
                    >
                      <div className="circle">
                        <User size={"16"} />
                      </div>
                    </NavLink>
                  ) : (
                    <NavLink className="nav-link">
                      <div className="circle">
                        <LogIn size={"16"} />
                      </div>
                    </NavLink>
                  )}
                </NavItem>
              </Nav>
            </Collapse>
          </Navbar>
        </HashRouter>
      </div>
    );
  }
}
