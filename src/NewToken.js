import React from "react";
import {
  Collapse,
  Container,
  Row,
  FormInput,
  Button,
  Col,
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  FormTextarea,
  Badge,
} from "shards-react";
import Travel from "./contracts/Travel.json";
import getContractInstance, { web3DataFetch } from "./utils/utils.js";
import { Map, TileLayer, Marker, Popup } from "react-leaflet";
// var ipfsClient = require("ipfs-http-client");
import LoginImage from "./assets/Image.webp";

import Arweave from "arweave";
import { key } from "./key";

// var ipfsClient = require("ipfs-http-client");

const arweave = Arweave.init({});

export default class NewToken extends React.Component {
  captureFile(event) {
    event.stopPropagation();
    event.preventDefault();
    this.setState({ added_file_hash: event.target.files });
  }

  runExample = async () => {
    const dataBuffer = await this.state.added_file_hash[0].arrayBuffer();
    let ipfsId;

    let transaction = await arweave.createTransaction(
      { data: dataBuffer },
      key
    );

    await arweave.transactions.sign(transaction, key);

    let uploader = await arweave.transactions.getUploader(transaction);

    while (!uploader.isComplete) {
      await uploader.uploadChunk();
      console.log(
        `${uploader.pctComplete}% complete, ${uploader.uploadedChunks}/${uploader.totalChunks}`
      );
    }

    if (uploader.isComplete) {
      // console.log(response);
      ipfsId = transaction.id;
      console.log(ipfsId);
      const { accounts, contract } = this.state;
      let content = JSON.stringify({
        subject: this.state.subject,
        details: this.state.details,
        ipfsId: ipfsId,
      });
      await contract.methods
        .add_posts(content, JSON.stringify(this.state.latlng))
        .send({ from: accounts[0] });
      this.setState({});

      alert("Place Added");
      window.location.reload();
    }

    //this.setState({subject:'',details:'',name:''}, this.getApplications);
    alert("Token Minted");
    window.location.reload();
  };

  componentDidMount = async () => {
    const obj = await web3DataFetch();
    this.setState({
      web3: obj.web3,
      accounts: obj.accounts,
      contract: obj.contract,
    });
  };

  mapRef = React.createRef();
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      details: "",
      web3: null,
      accounts: null,
      contract: null,
      hasLocation: false,
      latlng: {
        lat: 51.505,
        lng: -0.09,
      },
      added_file_hash: null,
    };

    this.handleInput = this.handleInput.bind(this);
    this.runExample = this.runExample.bind(this);
    this.captureFile = this.captureFile.bind(this);
  }

  handleClick = () => {
    const map = this.mapRef.current;
    if (map != null) {
      map.leafletElement.locate();
    }
  };

  handleLocationFound = (e) => {
    this.setState({
      hasLocation: true,
      latlng: e.latlng,
    });
  };

  handleInput(event) {
    const target = event.target;
    if (target.name == "name") {
      this.setState(Object.assign({}, this.state, { name: target.value }));
    } else if (target.name == "details") {
      this.setState(Object.assign({}, this.state, { details: target.value }));
    } else {
      this.setState(Object.assign({}, this.state, { name: target.value }));
    }
  }
  render() {
    const marker = this.state.hasLocation ? (
      <Marker position={this.state.latlng}>
        <Popup>You are here</Popup>
      </Marker>
    ) : null;
    return (
      <div>
        {this.state.contract ? (
          <Container className="main-container">
            <Row>
              <Col sm="12" md="12">
                <div className="form-body">
                  <img className="form-img" src={LoginImage} />
                  <Card className="card">
                    <CardBody>
                      <CardTitle className="title">
                        Enter The Details Of Place
                      </CardTitle>
                      <CardTitle className="label">Name of The Place</CardTitle>
                      <FormInput
                        name="name"
                        className="input"
                        placeholder="Name of Place"
                        value={this.state.name}
                        onChange={this.handleInput}
                      />
                      <br />
                      <CardTitle className="label">Where Is It</CardTitle>
                      <Map
                        center={this.state.latlng}
                        style={{ height: "15rem" }}
                        length={4}
                        onClick={this.handleClick}
                        onLocationfound={this.handleLocationFound}
                        ref={this.mapRef}
                        zoom={13}
                      >
                        <TileLayer
                          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        />
                        {marker}
                      </Map>
                      <br />
                      <CardTitle className="label">
                        Give Us Some Image
                      </CardTitle>
                      <FormInput
                        type="file"
                        theme="danger"
                        onChange={this.captureFile}
                        placeholder="Upload an Image"
                        className="form-control input"
                      />
                      <br />
                      <center>
                        <Button className="sbt-btn" onClick={this.runExample}>
                          Submit
                        </Button>
                      </center>
                    </CardBody>
                  </Card>
                </div>
              </Col>
            </Row>
          </Container>
        ) : (
          <div>Loading...</div>
        )}
      </div>
    );
  }
}
