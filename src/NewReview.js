import Arweave from "arweave";
import { gql, GraphQLClient } from "graphql-request";
import React from "react";
import { Map, Marker, Popup, TileLayer } from "react-leaflet";
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  Container,
  FormInput,
  FormTextarea,
  Row,
} from "shards-react";
// var ipfsClient = require("ipfs-http-client");
import LoginImage from "./assets/Image.webp";
import { key } from "./key";
import { GET_IDENTITY } from "./utils/subgraph";

var ipfsClient = require("ipfs-http-client");

const arweave = Arweave.init({});

export default class NewReview extends React.Component {
  captureFile(event) {
    event.stopPropagation();
    event.preventDefault();
    this.setState({ added_file_hash: event.target.files });
  }

  runExample = async () => {
    const dataBuffer = await this.state.added_file_hash[0].arrayBuffer();
    let arweaveHash;

    let transaction = await arweave.createTransaction(
      { data: dataBuffer },
      key
    );

    await arweave.transactions.sign(transaction, key);

    let uploader = await arweave.transactions.getUploader(transaction);

    alert("Uploading Image to Arweave... Please wait");
    while (!uploader.isComplete) {
      await uploader.uploadChunk();
      console.log(
        `${uploader.pctComplete}% complete, ${uploader.uploadedChunks}/${uploader.totalChunks}`
      );
    }

    if (uploader.isComplete) {
      alert(
        "Image Uploaded to Arweave, Please confirm transaction on MetaMask"
      );
      // console.log(response);
      arweaveHash = transaction.id;
      console.log(arweaveHash);
      const { accounts, contract } = this.props;
      let content = JSON.stringify({
        subject: this.state.subject,
        details: this.state.details,
        arweaveHash: arweaveHash,
      });

      await contract.methods
        .add_posts(content, JSON.stringify(this.state.latlng), arweaveHash)
        .send({ from: accounts[0] });
      this.setState({});

      alert("Place Added to Blockchain, Please go to Home to check it out");

      await this.props.fetchData();
    }

    console.log(this.state.latlng);
    this.setState({ subject: "", details: "", name: "" });
  };

  componentDidMount = async () => {
    // console.log("window.ethereum", window.ethereum);
    // const obj = await web3DataFetch();
    // const cyberConnect = new CyberConnect({
    //   namespace: "CyberConnect",
    //   env: Env.Production,
    //   chain: Blockchain.ETH,
    //   provider: obj.web3.currentProvider,
    //   signingMessageEntity: "CyberConnect" || "YOUR_ENTITY_HERE",
    // });
    // console.log("objasdfasd", obj);
    // cyberConnect.connect(obj.contract._address);
    // client
    //   .request(GET_CONNECTIONS, {
    //     address: obj.contract._address,
    //     first: 5,
    //   })
    //   .then((res) => {
    //     console.log("res: ", res);
    //   })
    //   .catch((e) => {});
  };

  mapRef = React.createRef();

  constructor(props) {
    super(props);
    this.state = {
      subject: "",
      details: "",
      hasLocation: false,
      latlng: {
        lat: 51.505,
        lng: -0.09,
      },
      added_file_hash: null,
    };

    this.handleInput = this.handleInput.bind(this);
    this.runExample = this.runExample.bind(this);
    this.captureFile = this.captureFile.bind(this);
  }

  handleClick = () => {
    const map = this.mapRef.current;
    if (map != null) {
      map.leafletElement.locate();
    }
  };

  handleLocationFound = (e) => {
    this.setState({
      hasLocation: true,
      latlng: e.latlng,
    });
  };

  handleInput(event) {
    const target = event.target;
    if (target.name == "subject") {
      this.setState(Object.assign({}, this.state, { subject: target.value }));
    } else if (target.name == "details") {
      this.setState(Object.assign({}, this.state, { details: target.value }));
    } else {
      this.setState(Object.assign({}, this.state, { name: target.value }));
    }
  }
  render() {
    const marker = this.state.hasLocation ? (
      <Marker position={this.state.latlng}>
        <Popup>You are here</Popup>
      </Marker>
    ) : null;
    return (
      <div>
        {this.props.contract ? (
          <Container className="main-container">
            <Row>
              <Col sm="12" md="12">
                <div className="form-body">
                  <img className="form-img" src={LoginImage} />

                  {/* <h3>Add New Place</h3>
                  <hr /> <br /> */}
                  <Card className="card">
                    <Row>
                      {/* <CardHeader>Enter The Details Of Place</CardHeader> */}
                      <CardBody>
                        <CardTitle className="title">Add New Place</CardTitle>
                        <CardTitle className="label">Catchy Title</CardTitle>
                        <FormInput
                          className="input"
                          name="subject"
                          placeholder="Name of Place"
                          value={this.state.subject}
                          onChange={this.handleInput}
                        />
                        <br />
                        <CardTitle className="label">
                          Why is it Awesome{" "}
                        </CardTitle>
                        <FormTextarea
                          className="input"
                          name="details"
                          value={this.state.details}
                          onChange={this.handleInput}
                          placeholder="Enter Place Details"
                        />
                        <br />
                        <CardTitle className="label">Where Is It</CardTitle>
                        <Map
                          style={{ height: "15rem" }}
                          center={this.state.latlng}
                          length={4}
                          onClick={this.handleClick}
                          onLocationfound={this.handleLocationFound}
                          ref={this.mapRef}
                          zoom={13}
                        >
                          <TileLayer
                            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                          />
                          {marker}
                        </Map>
                        <br />
                        <CardTitle className="label">
                          Give Us Some Images
                        </CardTitle>
                        <FormInput
                          type="file"
                          theme="danger"
                          onChange={this.captureFile}
                          placeholder="Upload an Image"
                          className="form-control input"
                        />
                        <br />
                        <center>
                          <Button className="sbt-btn" onClick={this.runExample}>
                            Submit
                          </Button>
                        </center>
                      </CardBody>
                    </Row>
                  </Card>
                </div>
              </Col>
            </Row>
          </Container>
        ) : (
          <div>Loading...</div>
        )}
      </div>
    );
  }
}
