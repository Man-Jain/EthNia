pragma solidity ^0.8.0;

import "./PostNFT.sol";

contract Travel {
    struct Traveller {
        uint256 tokens;
        uint256 repo;
        uint256 upvotes;
        uint256 downvotes;
        uint256 posts;
    }
    struct Post {
        string location;
        address publisher;
        string content;
        uint256 upvote_count;
        uint256 downvote_count;
        address[] downvoters;
        address[] upvoters;
        address nftAddress;
    }
    uint256 post_count;
    uint256 public tokenid;
    mapping(uint256 => Post) post_counter;
    mapping(uint256 => address) post_address;
    mapping(address => Traveller) address_traveller;

    function add_posts(
        string memory content,
        string memory location,
        string memory tokenURI
    ) public {
        post_counter[post_count].location = location;
        post_counter[post_count].publisher = msg.sender;
        post_counter[post_count].content = content;
        post_address[post_count] = msg.sender;

        ERC721URIStorage postToken = new PostNFT(msg.sender, tokenURI);
        post_counter[post_count].nftAddress = address(postToken);

        address_traveller[msg.sender].posts++;
        post_count++;
    }

    // function mint_token(string memory tokenURI) public {
    //     _mint(msg.sender, tokenid);
    //     _setTokenURI(tokenid, tokenURI);
    //     tokenid += 1;
    //     address_traveller[msg.sender].tokens++;
    // }

    function upvote(uint256 postId) public {
        bool status = inArray(msg.sender, postId);
        if (status) {
            post_counter[postId].upvoters.push(msg.sender);
            address_traveller[post_address[postId]].repo++;
            post_counter[postId].upvote_count++;
            address_traveller[msg.sender].upvotes++;
        } else {
            revert("already voted");
        }
    }

    function downvote(uint256 postId) public {
        bool status = inArray(msg.sender, postId);
        if (status) {
            post_counter[postId].downvoters.push(msg.sender);
            address_traveller[post_address[postId]].repo--;
            post_counter[postId].downvote_count++;
            address_traveller[msg.sender].downvotes++;
        } else {
            revert("already voted");
        }
    }

    function inArray(address who, uint256 id) private view returns (bool) {
        for (uint256 i = 0; i < post_counter[id].upvote_count; i++) {
            if (post_counter[id].upvoters[i] == who) {
                return false;
            }
        }
        for (uint256 i = 0; i < post_counter[id].downvote_count; i++) {
            if (post_counter[id].downvoters[i] == who) {
                return false;
            }
        }
        return true;
    }

    function getPostCount() public view returns (uint256 count) {
        count = post_count;
    }

    function getPosts(uint256 postId)
        public
        view
        returns (
            address addr,
            string memory location,
            string memory content,
            uint256 upvotes,
            uint256 downvotes,
            address nftAddress
        )
    {
        addr = post_counter[postId].publisher;
        content = post_counter[postId].content;
        location = post_counter[postId].location;
        upvotes = post_counter[postId].upvote_count;
        downvotes = post_counter[postId].downvote_count;
        nftAddress = post_counter[postId].nftAddress;
    }

    function myProfile()
        public
        view
        returns (
            uint256 tokens,
            uint256 repo,
            uint256 upvotes,
            uint256 downvotes,
            uint256 posts
        )
    {
        tokens = address_traveller[msg.sender].tokens;
        repo = address_traveller[msg.sender].repo;
        upvotes = address_traveller[msg.sender].upvotes;
        downvotes = address_traveller[msg.sender].downvotes;
        posts = address_traveller[msg.sender].posts;
    }

    function getProfile(address addr)
        public
        view
        returns (
            uint256 tokens,
            uint256 repo,
            uint256 upvotes,
            uint256 downvotes,
            uint256 posts
        )
    {
        tokens = address_traveller[addr].tokens;
        repo = address_traveller[addr].repo;
        upvotes = address_traveller[addr].upvotes;
        downvotes = address_traveller[addr].downvotes;
        posts = address_traveller[addr].posts;
    }
}
