pragma solidity ^0.8.0;

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC721/extensions/ERC721URIStorage.sol";

contract PostNFT is ERC721URIStorage {
    constructor(address publisher, string memory tokenURI)
        ERC721("Post Token", "P")
    {
        _mint(publisher, 0);
        _setTokenURI(0, tokenURI);
    }
}
