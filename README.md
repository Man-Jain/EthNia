# ETHNIA - Welcome to Ethnicity

Most of the tourists comes to the city , visit a few well known places and departs before even getting to see the real ethnicity and the places which does not have well sung sagas for them.

A community of travellers, proof of presence on an awesome place, incentivised support from local people and a review system based on reputation, all these combined would give us an opportunity to get all the nooks and corners of the city explored by tourists.

We have tapped into **CyberConnect's** Profile and Connection systems to build add the amazing features of following, liking and reporting posts, Connection Social Media profiles to ethereum accounts and the recommendation / posts feeds.

The **smart contracts** were developed using **ChainIDE** and the images as well as the website are uploaded on **Arweave** making the platform completely decentralized and resistant to censorship.

For detailed information please checkout the presentation [Presentation here](https://docs.google.com/presentation/d/1NRG9JnlBMy1oT-5tYfwuPytZlMY01GlnujEx9XmLNN8/edit#slide=id.p11)

## Features :-

- See a list of places near you, a feed of awesome places
- Add a new place if you have got one
- Get a NFT for places you have visited to show off
- Upvote and Downvote places to change their rankings
- Like and Report Places, improve the recommendation algorithms
- See Your profile which list a list of places you have added, your token and other cool stats
- Follow People and keep a track of the places they have visited, to make your own personal list

## Hosting and Deployment

**Hosted on Arweave here** - https://arweave.net/GCUHqpvjyIlDyIOGdYlNt2xmaPdj4Fhumrtt1MSlYnU
**Note: - Please make sure you are connected to one of the supported networks on Metamask - NEAR Mainnet, Moonbase Alpha or Polygon Mumbai**

**Demo Video** - https://youtu.be/mZC_Dj0L9qI

**Screenshots** - https://imgur.com/a/5nyHmbc

The dapp smart contracts are completely _cross-chain_ and deployed on -

- NEAR Mainnet - [https://aurorascan.dev/address/0x06c69fa0e25dede0d25865fa795448fa4851dfb9](https://aurorascan.dev/address/0x06c69fa0e25dede0d25865fa795448fa4851dfb9)
- Moonbase Alpha - [https://moonbase.moonscan.io/address/0xde7f37bbe563fb19de1e11e2449fccaa88cd1ba9](https://moonbase.moonscan.io/address/0xde7f37bbe563fb19de1e11e2449fccaa88cd1ba9)
- Mumbai Polygon - [https://mumbai.polygonscan.com/address/0x609D0a1b36b7B2dfF130aa452A6Ee91d461E1266](https://mumbai.polygonscan.com/address/0x609D0a1b36b7B2dfF130aa452A6Ee91d461E1266)

## Team

### Manank Patni

Blockchain Developer working on platforms like Ethereum, Polygon, Polakdot, Solana and others for 4 years. Have professional experience with Smart Contracts, Solidity, EVM and other blockchain technology suites.

**Github** - https://github.com/Man-Jain
**LinkedIn** - https://www.linkedin.com/in/manankpatni
**Gitcoin** - https://gitcoin.co/man-jain

### Kanika Agrawal

Kanika is a front-end developer. She have experience with technologies such as ReactJs, HTML, CSS, and React Native. She is currently working as Frontend Developer with Spheron Networks. She have been part of Demodyfi as Lead Frontend Developer and worked with the backend and design team. She have also worked with Wednesday Solutions as a Frontend developer intern, AnkTech Softwares for 2 months as a MEAN Stack developer trainee, and Decision Fiction for 1 month as a Frontend developer intern. She likes to participate in hackathons, and have participated in a fair amount of hackathons in her college as well as on online platforms such as Gitcoin and Devfolio and also won a few. Some of her winning hackathons include Smart India Hackathon Prelims, Take back the web(Wallet Connect Ticketing System), and NuCypher to protect privacy.

**Github** - https://github.com/Kanika1799
**LinkedIn** - https://www.linkedin.com/in/kanika17
**Gitcoin** - https://gitcoin.co/kanika1799

### `nvm use 12.22.0` - Change node version to 12.22.0

### `npm install` - Install the dependencies

### `npm start` - Run the Local Server

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser
